# 开放平台门户网站


<a href="https://www.iviewui.com/docs/guide/install" target="_blank">iview文档</a>


<a href="http://www.openc.top" target="_blank">站点预览地址</a>

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run dev
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
