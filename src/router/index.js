import Vue from 'vue'
import Router from 'vue-router'
const routes = [
    {
        path: '/',
        name: 'main',
        component: () => import('@/view/main.vue')
    },
    {
        path: '/home',
        name: 'home',
        component: () => import('@/view/home/index.vue')
    },
    {
        path: '/docs',
        name: 'docs',
        component: () => import('@/view/docs/index.vue')
    },
    {
    path: '*',
    name: '404',
    component: () => import('@/view/main.vue')
}
]

Vue.use(Router)

// 基础路由地址 和vue.config.js 中的baseUrl保持一致
const BASE_URL = process.env.NODE_ENV === 'production'
    ? '/'
    : '/'

const mainName = "main"

const router = new Router({
    base: BASE_URL,
    routes: routes,
    mode: 'history'
})



router.afterEach(to => {

})

export default router
